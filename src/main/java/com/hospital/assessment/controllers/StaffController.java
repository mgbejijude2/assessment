package com.hospital.assessment.controllers;

import com.hospital.assessment.dtos.requests.StaffRequest;
import com.hospital.assessment.dtos.responses.StaffDTO;
import com.hospital.assessment.service.StaffService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/staff")
public class StaffController {

    private final StaffService staffService;

    @PostMapping("/")
    public ResponseEntity<StaffDTO> create(@RequestBody StaffRequest staffRequest){
        return new ResponseEntity<>(staffService.createStaff(staffRequest), HttpStatus.CREATED);
    }

    @PatchMapping("/")
    public ResponseEntity<StaffDTO> update( @RequestBody StaffRequest staffRequest){
        return new ResponseEntity<>(staffService.update(staffRequest), HttpStatus.OK);
    }
}
