package com.hospital.assessment.security;

import com.hospital.assessment.entity.Users;
import com.hospital.assessment.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service("userDetailsService")
public class CustomUserDetailService implements UserDetailsService {

    private final UserService userService;

    private final Converter<Users, UserDetails> userUserDetailsConverter;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       Optional<Users> users= userService.getUserByUuid(username);
       if (!users.isPresent())
           throw new UsernameNotFoundException(username);

        return userUserDetailsConverter.convert(users.get());
    }
}
