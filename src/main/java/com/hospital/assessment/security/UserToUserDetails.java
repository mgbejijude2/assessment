package com.hospital.assessment.security;

import com.hospital.assessment.entity.Users;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Collection;

@Component
public class UserToUserDetails implements Converter<Users, UserDetails> {
    @Override
    public UserDetails convert(Users user) {
        CustomUserDetails userDetails = new CustomUserDetails();
        if (user != null) {
            userDetails.setUsername(user.getUuid());
            userDetails.setPassword(user.getPassword());
            userDetails.setEnabled(true);
            Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("USER"));
            userDetails.setAuthorities(authorities);
        }
        return userDetails;
    }
}