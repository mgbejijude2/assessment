package com.hospital.assessment.service.impl;

import com.hospital.assessment.dtos.requests.StaffRequest;
import com.hospital.assessment.dtos.responses.StaffDTO;
import com.hospital.assessment.entity.Staff;
import com.hospital.assessment.entity.Users;
import com.hospital.assessment.exception.DataNotFoundException;
import com.hospital.assessment.repository.StaffRepository;
import com.hospital.assessment.service.StaffService;
import com.hospital.assessment.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class StaffServiceImpl implements StaffService {

    private final StaffRepository staffRepository;
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @Value("${default.password:password}")
    private String defaultPassword;


    @Override
    public StaffDTO createStaff(StaffRequest request) {
        Staff staff=staffRepository.save(Staff.builder().name(request.getName()).build());
        Users users = userService.createUser(Users.builder()
                .uuid(UUID.randomUUID().toString())
                .password(passwordEncoder.encode(defaultPassword))
                .staff(staff).build());
        return StaffDTO.builder()
                .id(staff.getId())
                .name(staff.getName())
                .uuid(users.getUuid())
                .defaultPassword(defaultPassword)
                .registrationDate(staff.getCreatedOn())
                .build();
    }

    @Override
    public StaffDTO update(StaffRequest request) {
        Staff staff = userService.getUserByUuid(userService.getLoggedInUserName()).orElseThrow(() -> new DataNotFoundException("Could not find data")).getStaff();

        if(Objects.nonNull(request) && !staff.getName().equalsIgnoreCase(request.getName())){
            staff.setName(request.getName());
            staffRepository.save(staff);
        }
        return StaffDTO.builder()
                .id(staff.getId())
                .name(staff.getName())
                .uuid(userService.getLoggedInUserName())
                .registrationDate(staff.getCreatedOn())
                .build();
    }
}
