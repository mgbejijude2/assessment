package com.hospital.assessment.service;

import com.hospital.assessment.entity.Users;

import java.util.Optional;

public interface UserService {
    Optional<Users> getUserByUuid(String uuid);
    Users createUser(Users users);
    String getLoggedInUserName();
}
