package com.hospital.assessment.service;

import com.hospital.assessment.dtos.requests.StaffRequest;
import com.hospital.assessment.dtos.responses.StaffDTO;

public interface StaffService {
    StaffDTO createStaff(StaffRequest request);
    StaffDTO update(StaffRequest request);
}
