package com.hospital.assessment.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "user")
public class Users extends BaseEntity{

    @Column(name = "uuid", updatable = false, nullable = false)
    private String uuid;

    private String password;

    @OneToOne
    private Staff staff;

}
