package com.hospital.assessment.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "patient")
public class Patient extends BaseEntity{
    private int age;
    private String name;
    private LocalDateTime lastVisitDate;

}
