package com.hospital.assessment.dtos.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class StaffDTO {
    private Long id;
    private String uuid;
    private String name;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String defaultPassword;
    private Date registrationDate;

}
