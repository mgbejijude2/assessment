package com.hospital.assessment.dtos.requests;

import lombok.Data;

@Data
public class StaffRequest {
    private String name;
}
